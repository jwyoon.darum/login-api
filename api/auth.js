/*jshint esversion: 6 */

var express = require('express');
var axios = require('axios');
var path = require('path');
var router = express.Router();
var User = require(path.join(__BASEDIR, 'models/User'));
var util = require(path.join(__BASEDIR, 'util'));
var jwt = require('jsonwebtoken');

const JWT_SECRET = util.JWT_SECRET;
const TOKEN_EXPIRE_TIME = process.env.TOKEN_EXPIRE_TIME || 1200;

// login
router.post('/login', function (req, res, next) {
	util.log("## /login -> validation check:" + req.body.username + "/"	+ req.body.password);
	var isValid = true;
	var validationError = {
		name: 'ValidationError',
		errors: {}
	};

	if (!req.body.username) {
		isValid = false;
		validationError.errors.username = {
			message: 'Username is required!'
		};
	}
	if (!req.body.password) {
		isValid = false;
		validationError.errors.password = {
			message: 'Password is required!'
		};
	}

	if (!isValid)
		return res.json(util.successFalse(validationError));
	else
		next();
}, function (req, res, next) {
	util.log("## /login -> do login");


	//BNK login
	var resCode = "99";
	var errorCode = "99999999";
	var errorMessage = errorCustomerMessage = "로그인 실패";
	var result_info = "";

	var ret_common_ = "";
	var ret_body_ = "";
	var ret_set_cookie = "";
	var BSC_ID = "";

	axios.defaults.withCredentials = true;

	var params = new URLSearchParams();
	params.append('ib20_cur_mnu', 'MWP000000000001');
	params.append('MNGR_MODE_YN', '1');
	params.append('BANK_MNGR_ID', 'SMART_DEV');
	params.append('LOGIN_TYPE', '2');
	params.append('CUST_ID', req.body.username);

	axios.post("https://dm.busanbank.co.kr/ib20/act/MBPLGI000001A00M"
	, params)
	.then((ret) => {
		if(ret.status == 200) {
			// login 결과값
			ret_set_cookie = ret.headers['set-cookie'];
			ret_common_ = ret.data._msg_._common_;
			ret_body_ = ret.data._msg_._body_;

			resCode = ret_common_.resCode;
			errorCode = ret_common_['error.code'];
			errorMessage = ret_common_['error.message'];
			errorCustomerMessage = ret_common_['error.customer.message'];
			result_info = resCode+"|"+errorCode+"|"+errorMessage+"|"+errorCustomerMessage;

		}

		// login 성공
		if(resCode=="00")
		{
			BSC_ID = ret_body_.BSC_ID;
			util.log("# login success");
			var payload = {
				UID: BSC_ID,
				resCode : resCode
			};
			console
				.log("# get UID:" + BSC_ID);
	
			var secretOrPrivateKey = JWT_SECRET;
			var options = {
				expiresIn: TOKEN_EXPIRE_TIME * 1
			};

			util.log("# token expire time:" + TOKEN_EXPIRE_TIME * 1 + "::" + options.expiresIn);
			
			jwt.sign(payload, secretOrPrivateKey, options, function (
				err, token) {
				util.log("# generate token");
				if (err) {
					util.log("# error !");
					return res.json(util.successFalse(err));
				}

				util.log("# token:" + token);
				util.log("# ret_set_cookie:" + ret_set_cookie);
				util.log("# result_info:" + result_info);
				
				let result = util.successTrue(token, ret_set_cookie, result_info);
				
				res.json(result);
			}); 

		}
		// login 실패
		else
		{
			util.log("# resCode => " + resCode);
			util.log("# errorCode => " + errorCode);
			util.log("# errorMessage => " + errorMessage);
			util.log("# errorCustomerMessage => " + errorCustomerMessage);
			util.log("# result_info => " + result_info);
			return res.json(util.successFalse(null, 
				'Username or Password is invalid! (errCode:'+resCode+')', result_info));
		}
	})
	.catch((err) => {
		console.error(err);
		return res.json(util.successFalse(err));

	});	


});

// me-
router.get('/me', util.isLoggedin, function (req, res, next) {
	User.findById(req.decoded._id).exec(function (err, user) {
		if (err || !user)
			return res.json(util.successFalse(err));
		res.json(util.successTrue(user));
	});
});

// refresh
router.get('/refresh', util.isLoggedin, function (req, res, next) {
	User.findById(req.decoded._id).exec(
		function (err, user) {
			if (err || !user)
				return res.json(util.successFalse(err));
			else {
				var payload = {
					_id: user._id,
					username: user.username,
					name: user.name
				};
				var secretOrPrivateKey = JWT_SECRET;
				var options = {
					expiresIn: 60 * 60 * 24
				};
				jwt.sign(payload, secretOrPrivateKey, options, function (
					err, token) {
					if (err)
						return res.json(util.successFalse(err));
					res.json(util.successTrue(token));
				});
			}
		});
});

module.exports = router;
